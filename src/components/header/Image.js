import styled from 'styled-components'

export const HeaderBackgroud = styled.section`
    background-color: #005F98; 
    width: 100vw;
    height: 85px;
    margin: 0 auto;
    position: absolute;
    left: 0;
    top: 0;
    z-index: 1;
`
export const Logo = styled.img`
    object-fit: contain;
    height: 42px;
    width: 42px;
    z-index: 2;
    margin-left: 32px;
`