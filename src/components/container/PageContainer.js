import React from 'react'
import styled from 'styled-components'

const PageContainerComponent = styled.div`
    margin: 20px auto;
    height: auto;
    width: 100%;
    display: grid;
    row-gap: 20px;
    align-content: start;
    justify-items: center;
    text-align: center;
`
export const PageContainer = (props) => {
    return (
        <PageContainerComponent>
            {props.children}
        </PageContainerComponent>
    )
}