import styled from 'styled-components'

export const HeaderContainer = styled.div`
    margin: 0 auto;
    max-width: 1280px;
    height: 85px;
    display: grid;
    align-items: center;
    justify-content: start;
    grid-auto-flow: column;
    column-gap: 21.5px;
`