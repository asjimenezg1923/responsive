import React from 'react'
import ReactDOM from 'react-dom'
import {App} from './App'
import './index.css'
import swDev from './swDev'

ReactDOM.render(
    <App />,
  document.getElementById('root')
)

swDev();